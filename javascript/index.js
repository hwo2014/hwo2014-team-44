var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var Throttle = require('./throttle'),
    POI = require('./poi'),
    Utils = require('./utilities'),
    Logger = require('./logger');

var sendSpeed = function(data) {
  var track = Throttle.getTrack(),
      myCarData = Utils.getMyCar(data);
  
  var lane;
  track.lanes.some(function(oneLane) {
    if (oneLane.index === myCarData.piecePosition.lane.endLaneIndex) {
      lane = oneLane;
      return true;
    }
  });

  myCarData.gameTick = data.gameTick;
  myCarData.lane = lane;
  
  var throttleData = {
    currentSpeed: Throttle.getSpeed(myCarData, track.pieces),
    currentPiece: Utils.getCurrentPiece(myCarData.piecePosition.pieceIndex, track.pieces),
    currentInPieceDistance: myCarData.piecePosition.inPieceDistance,
    lane: lane,
    deceleration: 0.5,
    pieces: track.pieces,
    pois: POI.getPoisForPieces(track.pieces)
  };
  
  var throttle = Throttle.getThrottlePosition(throttleData);
  
  Logger.logState({
    tick: data.gameTick,
    currentSpeed: throttleData.currentSpeed,
    currentPiece: throttleData.currentPiece,
    currentInPieceDistance: throttleData.currentInPieceDistance,
    deceleration: throttleData.deceleration,
    // pieces: throttleData.pieces,
    // pois: throttleData.pois,
    throttle: throttle,
  });

  send({
    msgType: "throttle",
    data: throttle
  });
}

jsonStream.on('data', function(message) {
  var data = message.data;
  if (message.msgType === 'carPositions') {
    sendSpeed(message);
  } else {
    switch (message.msgType) {
      case 'join':
        console.log('Joined');
        Logger.start();
        break;
      case 'yourCar':
        console.log('Your car');
        Utils.setCarColor(data.color);
        break;
      case 'gameInit':
        console.log('Game init');
        Throttle.setTrack(data.race.track);
        break;
      case 'gameStart':
        console.log('Race started');
        break;
      case 'gameEnd':
        console.log('Race ended', data);
        Logger.end();
        break;
      case 'lapFinished':
        if (data.car.color === Utils.getCarColor()) {
          console.log('Lap finished, laptime', data.lapTime.millis);
        }
        break;
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
