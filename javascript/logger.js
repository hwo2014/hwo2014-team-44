var fs = require('fs'),
    Utils = require('./utilities');

var _events = [],
    _startTime,
    _appender;

var start = function() {
  console.log('Logging started');
  _startTime = new Date();
  var handle = fs.openSync('appending_log.csv', 'w');
  fs.writeSync(handle, 'tick,pieceIndex,inPieceDistance,speed,throttle,deceleration\n');
  fs.closeSync(handle);
  _appender = fs.createWriteStream('appending_log.csv', {'flags': 'a'});
};

var log = function(prop, data) {
  if (!_events[prop]) _events[prop] = [];
  _events[prop].push(data);
};

var logState = function(event) {
  _events.push(event);

  var line = [event.tick || 0,
         event.currentPiece.index,
         event.currentInPieceDistance,
         event.currentSpeed,
         event.throttle,
         event.deceleration,
         ].join(',');

  _appender.write(line + '\n');
  console.log(event.tick
    + ': speed = ' + event.currentSpeed
    + ', throttle = ' + event.throttle
    + ', piece index = ' + event.currentPiece.index + (Utils.isCurve(event.currentPiece) ? ' (curve)' : ''));
};

var end = function() {
  var prop, log = 'tick,currentPiece,currentInPieceDistance,currentSpeed,throttle,deceleration';

  _events.forEach(function(event, i) {
    var line = [event.tick,
           event.currentPiece.index,
           event.currentInPieceDistance,
           event.currentSpeed,
           event.throttle,
           event.deceleration,
           ].join(',');
          
    log = log + '\n' + line;
  })

  fs.writeFile('RACELOG_' + _startTime.toISOString() + '.txt', log, function(err) {
    if (err) {
      console.log('Error saving log:', err);
    }
  });

  console.log('Logging ended');
};

module.exports = {
  start: start,
  log: log,
  logState: logState,
  end: end
};