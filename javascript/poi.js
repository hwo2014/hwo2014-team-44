var Utils = require('./utilities'),
    Logger = require('./logger');

// Holds the POIs for the current track. Stateful, but required.
var _pois;

var MIN_CURVE_SPEED = 0.2,
    MAX_CURVE_SPEED = 0.65;

// Calculates the distance from current piece & dist in piece to the POI
var getDistanceToPoi = function(currentPiece, currentInPieceDistance, poi, lane, pieces) {
  return Utils.getDistanceBetween(currentPiece, currentInPieceDistance, poi, 0, lane, pieces);
};

var getMaxSpeedAtPoi = function(poi, lane) {
  if (Utils.isStraight(poi)) return 1;

  // This is the magic that we probably need to adjust!
  var curveLength = Utils.getPieceLength(poi, 0, lane),
      tightness = 1 - Math.abs(poi.angle) / curveLength;
      maxSpeed = Utils.clip(tightness, MIN_CURVE_SPEED, MAX_CURVE_SPEED);

  return maxSpeed; // 0.1
};

var getPoisForPieces = function(pieces) {
  // Lazily initialize pois
  if (!_pois) {
    _pois = _calculatePois(pieces);
  }
  return _pois;
};

// Stupidly defines POIs. Currently if a piece
// is a curve, it's a POI.
var _calculatePois = function(pieces) {
  var pois = [];

  pieces.forEach(function(piece, i) {
    if (Utils.isCurve(piece)) {
      piece.index = i;
      pois.push(piece);
    }
  });

  return pois;
};

module.exports = {
  getDistanceToPoi: getDistanceToPoi,
  getMaxSpeedAtPoi: getMaxSpeedAtPoi,
  getPoisForPieces: getPoisForPieces
};