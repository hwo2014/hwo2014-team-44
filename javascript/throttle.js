var POI = require('./poi'),
    Utils = require('./utilities'),
    Logger = require('./logger');

var _track;

var setTrack = function(t) {
  _track = t;
};

var getTrack = function() {
  return _track;
};

var _prevTick = 0,
    _prevDistInPiece = 0,
    _prevPiece;

var getSpeed = function(data, pieces) {
  if (!_prevPiece) {
    // First iteration, no prevPiece set yet
    _prevPiece = pieces[data.piecePosition.pieceIndex];
    return 0;
  }
  
  var nextPiece = pieces[data.piecePosition.pieceIndex];
  nextPiece.index = data.piecePosition.pieceIndex;
  //console.log(data);
  var speed = Utils.getSpeed(_prevPiece,
                             _prevDistInPiece,
                             _prevTick,
                             nextPiece,
                             data.piecePosition.inPieceDistance,
                             data.gameTick,
                             data.lane,
                             pieces);
  
  _prevTick = data.gameTick;
  _prevDistInPiece = data.piecePosition.inPieceDistance;
  _prevPiece = pieces[data.piecePosition.pieceIndex];

  return speed;
};

var getThrottlePosition = function(data) {
  var currentSpeed = data.currentSpeed,
      currentPiece = data.currentPiece,
      currentInPieceDistance = data.currentInPieceDistance,
      deceleration = data.deceleration,
      pieces = data.pieces,
      pois = data.pois,
      lane = data.lane;
  
  var throttle = 1;

  pois.some(function(poi, i) {
    var distanceToPoi = POI.getDistanceToPoi(currentPiece, currentInPieceDistance, poi, lane, pieces);

    if (distanceToPoi < 0) return false;

    var maxSpeedAtPoi = POI.getMaxSpeedAtPoi(poi, lane),
        decelerationDist = Math.pow(maxSpeedAtPoi - currentSpeed, 2) / (2 * deceleration);
    if (maxSpeedAtPoi < currentSpeed && distanceToPoi <= decelerationDist) {
      // Need to break.
      throttle = 0;
      // Shortcircuits some, breaking out from loop
      return true;
    }

    throttle = 1;
  });
  return throttle;
};

module.exports = {
  getThrottlePosition: getThrottlePosition,
  setTrack: setTrack,
  getTrack: getTrack,
  getSpeed: getSpeed
};