// Holds the color of our car. Stateful, but required.
var _color;

var setCarColor = function(c) {
  console.log("Car color set to", c);
  _color = c;
};

var getCarColor = function() {
  return _color;
}

var isCurve = function(piece) {
  return piece.radius !== undefined;
};

var isStraight = function(piece) {
  return !isCurve(piece);
};

var getMyCar = function(carPositions) {
  return _findMyCar(carPositions, _color);
};

// Injects the index into the current piece so that
// the piece consists of
// {
//   length: 100, // (in case of a straight)
//   index: 3
// }
var getCurrentPiece = function(index, pieces) {
  var piece = pieces[index];
  piece.index = index;

  return piece;
};

// Calculates current speed based on the difference
// in position and time
var getSpeed = function(prevPiece, prevDistInPiece, prevTick, currPiece, currDistInPiece, currTick, lane, pieces) {
  var tickDelta = currTick - prevTick,
      distDelta = getDistanceBetween(prevPiece, prevDistInPiece, currPiece, currDistInPiece, lane, pieces);
  if (distDelta < 0) return 0;
  return distDelta / tickDelta;
};

// Calculates distance between two pieces, taking distInPiece
// into account.
var getDistanceBetween = function(startPiece, startPieceDistInPiece, endPiece, endPieceDistInPiece, lane, pieces) {
  var pieceDelta = (endPiece.index - startPiece.index + pieces.length) % pieces.length,
      distance = endPieceDistInPiece - startPieceDistInPiece,
      i;
  // Consider the exception where on the same piece.
  if (pieceDelta === 0 && distance < 0) pieceDelta = pieces.length;

  for (i = 0; i < pieceDelta; i++) {
    var piece = pieces[(startPiece.index + i) % pieces.length];
    distance += getPieceLength(piece, 0, lane);
  }
  
  if (distance < 0) {
    console.log('WARN: Negative distance:', distance);
    console.log('For parameters:', distance);
    console.log(startPiece, startPieceDistInPiece, endPiece, endPieceDistInPiece, pieces)
    for (i = 0; i < pieceDelta; i++) {
      var piece = pieces[(startPiece.index + i) % pieces.length];
      console.log((startPiece.index, i), ':', getPieceLength(piece, 0, lane))
    }
  }
  
  return distance;
};

var getPieceLength = function(piece, distInPiece, lane) {
  if (isStraight(piece)) {
    return piece.length - distInPiece;
  } else {
    var d = lane.distanceFromCenter * (piece.angle < 0 ? 1 : -1);
    return (piece.radius + d) * _degToRad(Math.abs(piece.angle)) - distInPiece;
  }
};

var clip = function(val, min, max) {
  if (val > max) {
    return max;
  } else if (val < min) {
    return min;
  } else {
    return val;
  }
};

var _degToRad = function(deg) {
  return deg * (Math.PI / 180);
};

var _radToDeg = function(rad) {
  return rad * (180 / Math.PI);
};

// Picks "our car" based on the myColor argument
// from the carPositions server reply
var _findMyCar = function(carPositions, myColor) {
  var car;

  carPositions.data.some(function(carData) {
    if (carData.id.color === myColor) {
      car = carData;
      return true;
    }
  });

  return car;
};

module.exports = {
  isCurve: isCurve,
  isStraight: isStraight,
  getSpeed: getSpeed,
  getMyCar: getMyCar,
  getDistanceBetween: getDistanceBetween,
  getCurrentPiece: getCurrentPiece,
  getPieceLength: getPieceLength,
  setCarColor: setCarColor,
  getCarColor: getCarColor,
  clip: clip
};