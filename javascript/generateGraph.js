#!/usr/bin/env node

var fs = require('fs');

if (process.argv.length < 3) {
  console.log("Usage: ./generateGraph.js 2014-04-27T12:54:44.717Z");
  return;
}

var date = process.argv.slice(2)[0];

var graphTemplate = require('./graphTemplate');

fs.readdirSync(__dirname)
  .filter(function(fileName) {
    return fileName.indexOf('RACELOG_' + date) === 0;
  })
  .forEach(function(logFileName) {
    var file = fs.readFileSync(logFileName, 'utf8').split('\n'),
        lines = file.map(function(val, i) {
          return i + ',' + val;
        }),
        fileNameParts = logFileName.split('_'),
        header = fileNameParts[fileNameParts.length - 1];
    lines.unshift('line,' + header);

    var str = lines.join("\\n");
    var page = graphTemplate.replace('{{ data }}', str);

    fs.writeFileSync('GRAPH_' + date + '_' + header + '.html', page);
  });
