module.exports = 
  '<!doctype html>' +
  '<html lang="en">' +
  '  <head>' +
  '    <meta charset="utf-8">' +
  '    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">' +
  '    <title></title>' +
  '    <meta name="viewport" content="width=device-width,initial-scale=1">' +
  '    <script src="http://dygraphs.com/dygraph-combined.js"></script>' +
  '  </head>' +
  '  <body>' +
  '    <div id="graph"></div>' +
  '    <script>' +
  '      var data = "{{ data }}";' +
  '      var g = new Dygraph(document.getElementById("graph"), data);' +
  '    </script>' +
  '  </body>' +
  '</html>';
